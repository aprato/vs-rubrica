package com.rubrica.person;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.web.client.RestTemplate;

import client.rubrica.IPersonRestTemplateService;
import client.rubrica.PersonRestTemplateService;

@SpringBootTest
public class HttpRequestTest {

	private final RestTemplate restTemplate = new RestTemplate();
	
	private final IPersonRestTemplateService service = new PersonRestTemplateService(restTemplate); 
	
	@Test
	public void getToken() {
		service.getToken();
	}
	
	@Test
	public void getAll() {
		service.getAllPersons();
	}
	
	@Test
	public void addNew() {	
		service.addNewPerson();
	}
	
	@Test
	public void updateById() {
		service.updatePersonById();
	}
	
	@Test
	public void deleteById() {
		service.deletePersonById();
	}
}
