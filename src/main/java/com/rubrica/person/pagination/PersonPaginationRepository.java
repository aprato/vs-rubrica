package com.rubrica.person.pagination;

import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.rubrica.person.Person;


@Repository
public interface PersonPaginationRepository extends PagingAndSortingRepository<Person, Long> {
	
	Person getById(long id);
	
	List<Person> findByNameContainingIgnoreCase(String name);
	
	Person deleteById(long id);
}
