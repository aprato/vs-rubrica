package com.rubrica.person.pagination;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.rubrica.person.Person;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class PersonPaginationService {
	
	
	private final PersonPaginationRepository repository;
	
	public List<Person> getAllPersons(Integer pageNo, Integer pageSize){
		
		Pageable paging = PageRequest.of(pageNo, pageSize);
		
		Page<Person> pagedResult = repository.findAll(paging);
		
		if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<Person>();
        }
	}
	
	public Person getById(long id) {
		return repository.getById(id);
	}
	
	public List<Person> findByNameContainingIgnoreCase(String name) {
		return repository.findByNameContainingIgnoreCase(name);
	}
	
	public Person deleteById(long id) {
		return repository.deleteById(id);
	}
	
	public Person save(Person newPerson) {
		return repository.save(newPerson);
	}
}
