package com.rubrica.person.pagination;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rubrica.person.Person;

import lombok.extern.slf4j.Slf4j;

@Profile("pagination")
@Slf4j
@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class RestControllerPersonPaginated {
	
	@Autowired
	private PersonPaginationService service;
	
	
	
	@GetMapping("/persons")
	public ResponseEntity<List<Person>> getAllPersons(
														@RequestParam(defaultValue = "0") Integer pageNo, 
														@RequestParam(defaultValue = "5") Integer pageSize){
		 List<Person> persons = service.getAllPersons(pageNo, pageSize);
		 return new ResponseEntity<List<Person>>(persons, new HttpHeaders(), HttpStatus.OK);
	}
	
	
	@GetMapping("persons/{id}")
	public ResponseEntity<Person> getById(@PathVariable long id){
		Person person = service.getById(id);
		return new ResponseEntity<>(person, HttpStatus.OK);
	}
	
	
	@GetMapping("persons/s/{name}")
	public ResponseEntity<List<Person>> getByName(@PathVariable String name){
		List<Person> person = service.findByNameContainingIgnoreCase(name);
		return new ResponseEntity<>(person, HttpStatus.OK);
	}
	
	
	@DeleteMapping("persons/{id}")
	public ResponseEntity<Person> deleteById(@PathVariable long id){
		Person person = service.deleteById(id);
		return new ResponseEntity<>(person, HttpStatus.OK);
	}
	
	
	@PostMapping("persons")
	public ResponseEntity<Person> create(@RequestBody Person newPerson,@RequestHeader("Authorization") String header){
		log.info("Header: {}",header);
		Person person = service.save(newPerson);
		return new ResponseEntity<>(person, HttpStatus.CREATED);
	}
	
	
	@PutMapping("persons/{id}")
	public ResponseEntity<Person> updatePersonById(@PathVariable long id, @RequestBody Person upPerson){
		Person person = service.getById(id);
		
		person.setName(upPerson.getName());
		person.setSurname(upPerson.getSurname());
		person.setNumber(upPerson.getNumber());
		
		Person updatePerson = service.save(person);
		return new ResponseEntity<>(updatePerson,HttpStatus.OK);
	}
}