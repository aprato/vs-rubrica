package com.rubrica.person;

public class PersonNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3099376190381704602L;

	private Long id;
	
	public PersonNotFoundException(Long id) {
		super("Person non trovato");
		this.id=id;
	}

	
	public Long getId() {
		return id;
	}
}
