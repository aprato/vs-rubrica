package com.rubrica.person;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "Persons")
public class Person {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "name")
	private String name;
	@Column(name = "surname")
	private String surname;
	@Column(name = "number")
	private String number;

	public Person() {
	}
	
	public Person(String name, String surname, String number) {
		this.name = name;
		this.surname = surname;
		this.number = number;
	}
	
	@Override
	public String toString() {
		return String.format("Person[id = %d, name = '%s', surname = '%s', number = '%s']",
		        id, name, surname, number);
	}
}