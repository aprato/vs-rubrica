package com.rubrica.person;

import lombok.Data;

public @Data class RestError {

	private Long id;
	private String description;
	private String className;
	
}
