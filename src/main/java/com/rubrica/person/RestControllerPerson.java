package com.rubrica.person;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@RequiredArgsConstructor
@Profile("!pagination")
@Slf4j
@RestController
@RequestMapping("/persons")
public class RestControllerPerson {
	
	private final PersonRepository repository;
	
	
	@GetMapping
	public ResponseEntity<List<Person>> getAllPersons(){
		 List<Person> persons = new ArrayList<>(); 
		 repository.findAll().forEach(persons::add);
		 return new ResponseEntity<List<Person>>(persons, new HttpHeaders(), HttpStatus.OK);
	}
	
	
	@GetMapping("/{id}")
	public ResponseEntity<Person> getById(@PathVariable long id){
		Optional<Person> res = repository.findById(id);
		Person person=res.isEmpty()?null:res.get();
		if (person==null) throw new PersonNotFoundException(id);
		return new ResponseEntity<>(person, HttpStatus.OK);
	}
	
	
	@GetMapping(params = {"name"})
	public ResponseEntity<List<Person>> getByName(@RequestParam("name") String name){
		List<Person> person = repository.findByNameContainingIgnoreCase(name);
		return new ResponseEntity<>(person, HttpStatus.OK);
	}
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Person> deleteById(@PathVariable long id){
		Person person = repository.deleteById(id);
		return new ResponseEntity<>(person, HttpStatus.OK);
	}
	
	
	@PostMapping
	public ResponseEntity<Person> create(@RequestBody Person newPerson,@RequestHeader("Authorization") String header){
		log.info("Header: {}",header);
		Person person = repository.save(newPerson);
		return new ResponseEntity<>(person, HttpStatus.CREATED);
	}
	
	
	@PutMapping("/{id}")
	public ResponseEntity<Person> updatePersonById(@PathVariable long id, @RequestBody Person upPerson){
		Person person = repository.getById(id);
		
		person.setName(upPerson.getName());
		person.setSurname(upPerson.getSurname());
		person.setNumber(upPerson.getNumber());
		
		Person updatePerson = repository.save(person);
		return new ResponseEntity<>(updatePerson,HttpStatus.OK);
	}
	
	
	@ExceptionHandler(PersonNotFoundException.class)
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	public RestError getError(PersonNotFoundException exc) {
		RestError error=new RestError();
		error.setId(exc.getId());
		error.setDescription(exc.getMessage());
		error.setClassName(exc.getClass().getCanonicalName());
		return error;
	}
	
}