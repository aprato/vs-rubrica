package com.rubrica.person;

import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {
	
	Person getById(long id);
	
	List<Person> findByNameContainingIgnoreCase(String name);
	
	Person deleteById(long id);
}