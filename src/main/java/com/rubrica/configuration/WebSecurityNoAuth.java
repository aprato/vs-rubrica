package com.rubrica.configuration;



import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Profile("!auth")
@Configuration
@EnableWebSecurity
public class WebSecurityNoAuth extends WebSecurityConfigurerAdapter	 {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
          .cors().disable()
          .authorizeRequests()
         .antMatchers("/**").permitAll();
    }


}