package com.rubrica;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import com.rubrica.person.Person;
import com.rubrica.person.PersonRepository;

import lombok.extern.slf4j.Slf4j;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Slf4j
@EnableSwagger2
@SpringBootApplication
public class RubricaApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(RubricaApplication.class);
	}

	@Bean
	public CommandLineRunner demo (PersonRepository repository) {
		return (args) -> {
			//save person
			repository.save(new Person("Andrea", "s1", "1"));
			repository.save(new Person("Mario", "s2", "2"));
			repository.save(new Person("Giorgio", "s3", "3"));
			repository.save(new Person("Gianni", "s4", "4"));
			repository.save(new Person("Giuseppe", "s5", "5"));
			repository.save(new Person("Pietro", "s6", "6"));
		};
	}
	
}