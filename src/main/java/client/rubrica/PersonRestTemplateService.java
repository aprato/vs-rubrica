package client.rubrica;

import java.nio.charset.Charset;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.web.client.RestTemplate;

import com.rubrica.person.Person;

public class PersonRestTemplateService implements IPersonRestTemplateService {
	
	private static final String GET_ALL = "http://localhost:8080/persons";
	
	private static final String GET_BY_ID = "http://localhost:8080/persons/{id}";
	
	
	private final RestTemplate restTemplate;
	
	@Value("$spring.datasource.username}")
	private String username;
	
	@Value("${spring.datasource.password}")
	private String password;
	
	public PersonRestTemplateService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	private HttpHeaders createHeaders(String username, String password){
		return new HttpHeaders() {{
			String auth = username + ":" + password;
		    byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(Charset.forName("US-ASCII")) );
		    String authHeader = "Basic " + new String( encodedAuth );
		    set( "Authorization", authHeader );
		}};
	}
	
	
	@Override
	public void getToken() {
		Map<String, String> map = new HashMap<>();
		map.put("grant_type", "client_credentials");
		
		ResponseEntity<Void> res = restTemplate.exchange("https://rwil-qa.volkswagenag.com/rwil/gedai/oauth2/token", 
				HttpMethod.POST, 
				new HttpEntity<>("grant_type: client_credentials", createHeaders("ITA264-INFINITY-DMS-Q", "oJJLezuL16kWBKXDo3Fr24eaAVcJc3uP")),
				Void.class);
		System.out.println(res);
	}
	
	@Override
	public void getAllPersons() {
		ResponseEntity<List<Person>> responseEntity = restTemplate.exchange(GET_ALL, HttpMethod.GET, new HttpEntity<>(createHeaders("user1", "user1Pass")), new ParameterizedTypeReference<List<Person>>() {});
		System.out.println(responseEntity);
	}
	
	@Override
	public void addNewPerson() {
		Person newPerson = new Person("n4", "s4", "4");
		restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("user1", "user1Pass"));
		
		Person result = restTemplate.postForObject(GET_ALL, newPerson, Person.class);
		System.out.println(result);
	}	
	
	@Override
	public void updatePersonById() {
		Map<String,String> params = new HashMap<String,String>();
		params.put("id", "1");
		
		restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("user1", "user1Pass"));
		
		Person updataPerson = new Person("n0","s0","0");
		restTemplate.put(GET_BY_ID, updataPerson, params);
		System.out.println(updataPerson);
	}
	
	@Override
	public void deletePersonById() {
		Map<String,String> params = new HashMap<String,String>();
		params.put("id", "1");
		
		restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("user1", "user1Pass"));
		
		restTemplate.delete(GET_BY_ID, params);
		System.out.println("Utente cancellato");
	}
}