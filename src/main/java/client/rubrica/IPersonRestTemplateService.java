package client.rubrica;

public interface IPersonRestTemplateService {
	
	public void getToken();
	
	public void getAllPersons();
	
	public void addNewPerson();
	
	public void updatePersonById();
	
	public void deletePersonById();
	
}
