# vs-rubrica

Progetto per formazione Java

# Indirizzo swagger
[http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)

# Paginazione

Prerequisito
- Attivare il profilo **pagination**

Url di test:
[http://localhost:8080/persons?pageNo=0&pageSize=2](http://localhost:8080/persons?pageNo=0&pageSize=2)


# Security
Prerequisito:
- Attivare il profilo **auth**

# Rubrica
Link GitLab

- [https://gitlab.com/aprato/vs-rubrica.git](https://gitlab.com/aprato/vs-rubrica.git)